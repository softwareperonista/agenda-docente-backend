module gitlab.com/softwareperonista/agenda-docente-backend

// +heroku goVersion go1.16
go 1.16

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-chi/chi v1.5.4
	github.com/go-chi/cors v1.2.0
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.10.2
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5
)
