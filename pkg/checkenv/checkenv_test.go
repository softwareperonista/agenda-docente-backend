package checkenv_test

import (
	"os"
	"testing"

	"gitlab.com/softwareperonista/agenda-docente-backend/pkg/checkenv"
)

func TestCheckEmptyUnsetted(t *testing.T) {
	_, err := checkenv.Checkempty("UNSETTED")

	if err == nil {
		t.Errorf("The variable is not setted, should return error")
	}
}

func TestCheckEmpty(t *testing.T) {
	settedValue := "test"
	os.Setenv("SETTED", settedValue)
	returnedValue, err := checkenv.Checkempty("SETTED")

	if err != nil {
		t.Errorf("The variable is setted, should return not error")
	}

	if returnedValue != settedValue {
		t.Errorf("The variable value is '%s', the function returned '%s'", settedValue, returnedValue)
	}
}
