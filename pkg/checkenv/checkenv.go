package checkenv

import (
	"errors"
	"os"
)

func Checkempty(variable string) (string, error) {
	value := os.Getenv(variable)

	if len(value) == 0 {
		return "", errors.New(variable + " is not setted")
	}

	return value, nil
}
