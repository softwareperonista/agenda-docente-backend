package data

import (
	"context"
	"database/sql"

	"gitlab.com/softwareperonista/agenda-docente-backend/internal/models"
)

type ScheduleRepository struct {
	Data *Data
}

func (sr *ScheduleRepository) GetByPosition(ctx context.Context, pId uint) ([]models.Schedule, error) {
	q := `
		SELECT id, weekday, start_time, end_time, position
		FROM schedules
		WHERE position = $1;
	`

	rows, err := sr.Data.DB.QueryContext(ctx, q, pId)
	if err != nil {
		return nil, err
	}

	var schedules []models.Schedule
	for rows.Next() {
		var s models.Schedule
		rows.Scan(&s.Id, &s.Day, &s.Start, &s.End, &s.Position)
		schedules = append(schedules, s)
	}

	return schedules, nil
}

func (sr *ScheduleRepository) Create(ctx context.Context, tx *sql.Tx, s *models.Schedule) error {
	q := `
		INSERT INTO schedules (weekday, start_time, end_time, position)
		VALUES ($1, $2, $3, $4)
		RETURNING id;
	`

	row := tx.QueryRowContext(ctx, q, s.Day, s.Start, s.End, s.Position)

	err := row.Scan(&s.Id)
	if err != nil {
		return err
	}

	return nil
}

func (sr *ScheduleRepository) Update(ctx context.Context, tx *sql.Tx, id uint, s models.Schedule) error {
	q := `
		UPDATE schedules SET weekday = $1, start_time = $2, end_time = $3, position = $4
		WHERE id = $5; 
	`

	stmt, err := tx.PrepareContext(ctx, q)
	if err != nil {
		return err
	}

	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, s.Day, s.Start, s.End, s.Position, id)
	if err != nil {
		return err
	}

	return nil
}

func (sr *ScheduleRepository) Delete(ctx context.Context, tx *sql.Tx, id uint) error {
	q := `
		DELETE FROM shedules WHERE id = $1;
	`

	stmt, err := tx.PrepareContext(ctx, q)
	if err != nil {
		return err
	}

	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, id)
	if err != nil {
		return err
	}

	return nil
}
