package data_test

import (
	"errors"
	"fmt"
	"log"
	"net/http/httptest"
	"regexp"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"gitlab.com/softwareperonista/agenda-docente-backend/internal/data"
	"gitlab.com/softwareperonista/agenda-docente-backend/internal/models"
)

func createSchoolYearRepository() (data.SchoolYearRepository, sqlmock.Sqlmock) {
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	return data.SchoolYearRepository{Data: &data.Data{DB: db}}, mock
}

func TestSchoolYearGetAll(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		syr, mock := createSchoolYearRepository()

		mock.ExpectQuery(`SELECT id, year, grade, first_day, last_day, break_start, break_end 
							FROM schoolyears;`).
			WillReturnRows(sqlmock.NewRows([]string{"id", "year", "grade", "first_day", "last_day", "break_start", "break_end"}).
				AddRow(
					1,
					2021,
					"Secundaria",
					time.Date(2021, time.March, 1, 0, 0, 0, 0, time.UTC),
					time.Date(2021, time.December, 20, 0, 0, 0, 0, time.UTC),
					time.Date(2021, time.July, 9, 0, 0, 0, 0, time.UTC),
					time.Date(2021, time.July, 25, 0, 0, 0, 0, time.UTC)).
				AddRow(
					2,
					2021,
					"Universidad",
					time.Date(2021, time.March, 2, 0, 0, 0, 0, time.UTC),
					time.Date(2021, time.December, 21, 0, 0, 0, 0, time.UTC),
					time.Date(2021, time.July, 10, 0, 0, 0, 0, time.UTC),
					time.Date(2021, time.July, 26, 0, 0, 0, 0, time.UTC)))

		r := httptest.NewRequest("GET", "http://example.com/foo", nil)

		ctx := r.Context()
		schoolYears, err := syr.GetAll(ctx)
		if err != nil {
			t.Error(err.Error())
		}

		if len(schoolYears) != 2 {
			t.Error("len(students): %user. Should be 2", len(schoolYears))
		}

		schoolyear := schoolYears[0]
		if schoolyear.Id != 1 ||
			schoolyear.Year != 2021 ||
			schoolyear.Grade != "Secundaria" ||
			schoolyear.FirstDay != time.Date(2021, time.March, 1, 0, 0, 0, 0, time.UTC) ||
			schoolyear.LastDay != time.Date(2021, time.December, 20, 0, 0, 0, 0, time.UTC) ||
			schoolyear.BreakStart != time.Date(2021, time.July, 9, 0, 0, 0, 0, time.UTC) ||
			schoolyear.BreakEnd != time.Date(2021, time.July, 25, 0, 0, 0, 0, time.UTC) {
			t.Error("Wrong schoolyear[0] data")
		}

		schoolyear = schoolYears[1]
		if schoolyear.Id != 2 ||
			schoolyear.Year != 2021 ||
			schoolyear.Grade != "Universidad" ||
			schoolyear.FirstDay != time.Date(2021, time.March, 2, 0, 0, 0, 0, time.UTC) ||
			schoolyear.LastDay != time.Date(2021, time.December, 21, 0, 0, 0, 0, time.UTC) ||
			schoolyear.BreakStart != time.Date(2021, time.July, 10, 0, 0, 0, 0, time.UTC) ||
			schoolyear.BreakEnd != time.Date(2021, time.July, 26, 0, 0, 0, 0, time.UTC) {
			t.Error("Wrong schoolyear[1] data")
		}
	})

	t.Run("db error", func(t *testing.T) {
		syr, mock := createSchoolYearRepository()

		mock.ExpectQuery(`SELECT id, year, grade, first_day, last_day, break_start, break_end 
						  	FROM schoolyears;`).
			WillReturnError(errors.New(""))

		r := httptest.NewRequest("GET", "http://example.com/foo", nil)

		ctx := r.Context()
		_, err := syr.GetAll(ctx)
		if err == nil {
			t.Error("should return error")
		}
	})
}

func TestSchoolYearGetOne(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		syr, mock := createSchoolYearRepository()

		mock.ExpectQuery(regexp.QuoteMeta(
			`SELECT id, year, grade, first_day, last_day, break_start, break_end 
				FROM schoolyears
				WHERE id = $1;`)).
			WillReturnRows(sqlmock.NewRows([]string{"id", "year", "grade", "first_day", "last_day", "break_start", "break_end"}).
				AddRow(
					1,
					2021,
					"Secundaria",
					time.Date(2021, time.March, 1, 0, 0, 0, 0, time.UTC),
					time.Date(2021, time.December, 20, 0, 0, 0, 0, time.UTC),
					time.Date(2021, time.July, 9, 0, 0, 0, 0, time.UTC),
					time.Date(2021, time.July, 25, 0, 0, 0, 0, time.UTC)))

		r := httptest.NewRequest("GET", "http://example.com/foo", nil)

		ctx := r.Context()
		schoolyear, err := syr.GetOne(ctx, 1)
		if err != nil {
			t.Error(err.Error())
		}

		fmt.Println(schoolyear)
		if schoolyear.Id != 1 ||
			schoolyear.Year != 2021 ||
			schoolyear.Grade != "Secundaria" ||
			schoolyear.FirstDay != time.Date(2021, time.March, 1, 0, 0, 0, 0, time.UTC) ||
			schoolyear.LastDay != time.Date(2021, time.December, 20, 0, 0, 0, 0, time.UTC) ||
			schoolyear.BreakStart != time.Date(2021, time.July, 9, 0, 0, 0, 0, time.UTC) ||
			schoolyear.BreakEnd != time.Date(2021, time.July, 25, 0, 0, 0, 0, time.UTC) {
			t.Error("Wrong schoolyear data")
		}
	})

	t.Run("db error", func(t *testing.T) {
		syr, mock := createSchoolYearRepository()

		mock.ExpectQuery(regexp.QuoteMeta(
			`SELECT id, year, grade, first_day, last_day, break_start, break_end 
				FROM schoolyears
				WHERE id = $1;`)).
			WillReturnError(errors.New(""))

		r := httptest.NewRequest("GET", "http://example.com/foo", nil)

		ctx := r.Context()
		_, err := syr.GetOne(ctx, 1)
		if err == nil {
			t.Error("should return error")
		}
	})
}

func TestSchoolYearCreate(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		syr, mock := createSchoolYearRepository()

		mock.ExpectQuery(regexp.QuoteMeta(`INSERT INTO schoolyears (year, grade, first_day, last_day, break_start, break_end)
										VALUES($1, $2, $3, $4, $5, $6)
										RETURNING id`)).
			WillReturnRows(sqlmock.NewRows([]string{"id"}).
				AddRow(1))

		schoolyear := models.SchoolYear{
			Year:       2021,
			Grade:      "Secundaria",
			FirstDay:   time.Date(2021, time.March, 2, 0, 0, 0, 0, time.UTC),
			LastDay:    time.Date(2021, time.December, 21, 0, 0, 0, 0, time.UTC),
			BreakStart: time.Date(2021, time.July, 10, 0, 0, 0, 0, time.UTC),
			BreakEnd:   time.Date(2021, time.July, 26, 0, 0, 0, 0, time.UTC),
		}

		r := httptest.NewRequest("GET", "http://example.com/foo", nil)
		ctx := r.Context()
		err := syr.Create(ctx, &schoolyear)
		if err != nil {
			t.Error(err.Error())
		}
	})

	t.Run("db error", func(t *testing.T) {
		syr, mock := createSchoolYearRepository()

		mock.ExpectQuery(regexp.QuoteMeta(`NSERT INTO schoolyears (year, grade, first_day, last_day, break_start, break_end)
		VALUES($1, $2, $3, $4, $5, $6)
		RETURNING id`)).WillReturnError(errors.New(""))

		schoolyear := models.SchoolYear{
			Year:       2021,
			Grade:      "Secundaria",
			FirstDay:   time.Date(2021, time.March, 2, 0, 0, 0, 0, time.UTC),
			LastDay:    time.Date(2021, time.December, 21, 0, 0, 0, 0, time.UTC),
			BreakStart: time.Date(2021, time.July, 10, 0, 0, 0, 0, time.UTC),
			BreakEnd:   time.Date(2021, time.July, 26, 0, 0, 0, 0, time.UTC),
		}

		r := httptest.NewRequest("GET", "http://example.com/foo", nil)
		ctx := r.Context()
		err := syr.Create(ctx, &schoolyear)
		if err == nil {
			t.Error(err.Error())
		}
	})
}

func TestSchoolYearUpdate(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		syr, mock := createSchoolYearRepository()

		mock.ExpectPrepare(regexp.QuoteMeta(`UPDATE schoolyears in year=$1, grade=$2, first_day=$3, last_day=$4, break_start=$5, break_end=$6
	WHERE id=$7;`)).
			WillBeClosed().ExpectExec().WithArgs(
			2021,
			"Secundaria",
			time.Date(2021, time.March, 2, 0, 0, 0, 0, time.UTC),
			time.Date(2021, time.December, 21, 0, 0, 0, 0, time.UTC),
			time.Date(2021, time.July, 10, 0, 0, 0, 0, time.UTC),
			time.Date(2021, time.July, 26, 0, 0, 0, 0, time.UTC),
		).WillReturnResult(sqlmock.NewResult(1, 1))

		schoolyear := models.SchoolYear{
			Year:       2021,
			Grade:      "Secundaria",
			FirstDay:   time.Date(2021, time.March, 2, 0, 0, 0, 0, time.UTC),
			LastDay:    time.Date(2021, time.December, 21, 0, 0, 0, 0, time.UTC),
			BreakStart: time.Date(2021, time.July, 10, 0, 0, 0, 0, time.UTC),
			BreakEnd:   time.Date(2021, time.July, 26, 0, 0, 0, 0, time.UTC),
		}

		r := httptest.NewRequest("GET", "http://example.com/foo", nil)

		ctx := r.Context()
		err := syr.Update(ctx, uint(1), schoolyear)
		if err != nil {
			t.Error(err.Error())
		}

		err = mock.ExpectationsWereMet()
		if err != nil {
			t.Error(err.Error())
		}
	})

	t.Run("db error prepare", func(t *testing.T) {
		syr, mock := createSchoolYearRepository()

		mock.ExpectPrepare(regexp.QuoteMeta(`UPDATE schoolyears in year=$1, grade=$2, first_day=$3, last_day=$4, break_start=$5, break_end=$6
			WHERE id=$7;`)).WillReturnError(errors.New(""))

		schoolyear := models.SchoolYear{
			Year:       2021,
			Grade:      "Secundaria",
			FirstDay:   time.Date(2021, time.March, 2, 0, 0, 0, 0, time.UTC),
			LastDay:    time.Date(2021, time.December, 21, 0, 0, 0, 0, time.UTC),
			BreakStart: time.Date(2021, time.July, 10, 0, 0, 0, 0, time.UTC),
			BreakEnd:   time.Date(2021, time.July, 26, 0, 0, 0, 0, time.UTC),
		}

		r := httptest.NewRequest("GET", "http://example.com/foo", nil)

		ctx := r.Context()
		err := syr.Update(ctx, uint(1), schoolyear)
		if err == nil {
			t.Error(err.Error())
		}

		err = mock.ExpectationsWereMet()
		if err != nil {
			t.Error(err.Error())
		}
	})

	t.Run("db error exec", func(t *testing.T) {
		syr, mock := createSchoolYearRepository()

		mock.ExpectPrepare(regexp.QuoteMeta(`UPDATE schoolyears in year=$1, grade=$2, first_day=$3, last_day=$4, break_start=$5, break_end=$6
			WHERE id=$7;`)).WillBeClosed().ExpectExec().WithArgs(
			2021,
			"Secundaria",
			time.Date(2021, time.March, 2, 0, 0, 0, 0, time.UTC),
			time.Date(2021, time.December, 21, 0, 0, 0, 0, time.UTC),
			time.Date(2021, time.July, 10, 0, 0, 0, 0, time.UTC),
			time.Date(2021, time.July, 26, 0, 0, 0, 0, time.UTC),
		).WillReturnError(errors.New(""))

		schoolyear := models.SchoolYear{
			Year:       2021,
			Grade:      "Secundaria",
			FirstDay:   time.Date(2021, time.March, 2, 0, 0, 0, 0, time.UTC),
			LastDay:    time.Date(2021, time.December, 21, 0, 0, 0, 0, time.UTC),
			BreakStart: time.Date(2021, time.July, 10, 0, 0, 0, 0, time.UTC),
			BreakEnd:   time.Date(2021, time.July, 26, 0, 0, 0, 0, time.UTC),
		}

		r := httptest.NewRequest("GET", "http://example.com/foo", nil)

		ctx := r.Context()
		err := syr.Update(ctx, uint(1), schoolyear)
		if err == nil {
			t.Error(err.Error())
		}

		err = mock.ExpectationsWereMet()
		if err != nil {
			t.Error(err.Error())
		}
	})
}

func TestSchoolYearDelete(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		syr, mock := createSchoolYearRepository()

		mock.ExpectPrepare(regexp.QuoteMeta(`DELETE FROM schoolyears WHERE id = $1;`)).
			WillBeClosed().ExpectExec().WithArgs(1).WillReturnResult(sqlmock.NewResult(1, 1))

		r := httptest.NewRequest("GET", "http://example.com/foo", nil)

		ctx := r.Context()
		err := syr.Delete(ctx, uint(1))
		if err != nil {
			t.Error(err.Error())
		}

		err = mock.ExpectationsWereMet()
		if err != nil {
			t.Error(err.Error())
		}
	})

	t.Run("db error prepare", func(t *testing.T) {
		syr, mock := createSchoolYearRepository()

		mock.ExpectPrepare(regexp.QuoteMeta(`DELETE FROM schoolyears WHERE id = $1;`)).
			WillBeClosed().WillReturnError(errors.New(""))

		r := httptest.NewRequest("GET", "http://example.com/foo", nil)

		ctx := r.Context()
		err := syr.Delete(ctx, uint(1))
		if err == nil {
			t.Error(err.Error())
		}
	})

	t.Run("db error exec", func(t *testing.T) {
		syr, mock := createSchoolYearRepository()

		mock.ExpectPrepare(regexp.QuoteMeta(`DELETE FROM schoolyears WHERE id = $1;`)).
			WillBeClosed().ExpectExec().WithArgs(1).WillReturnError(errors.New(""))

		r := httptest.NewRequest("GET", "http://example.com/foo", nil)

		ctx := r.Context()
		err := syr.Delete(ctx, uint(1))
		if err == nil {
			t.Error(err.Error())
		}

		err = mock.ExpectationsWereMet()
		if err != nil {
			t.Error(err.Error())
		}
	})
}
