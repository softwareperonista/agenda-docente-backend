package data

import (
	"database/sql"
	"log"

	_ "github.com/lib/pq"
	"gitlab.com/softwareperonista/agenda-docente-backend/internal/database"
	"gitlab.com/softwareperonista/agenda-docente-backend/pkg/checkenv"
)

func getConnection() (*sql.DB, error) {
	uri, err := checkenv.Checkempty("DATABASE_URL")
	if err != nil {
		log.Fatal(err)
	}

	return sql.Open("postgres", uri)
}

func MakeMigration(db *sql.DB) error {
	b := database.Model()

	rows, err := db.Query(string(b))
	if err != nil {
		return err
	}

	return rows.Close()
}
