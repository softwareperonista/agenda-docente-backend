package data_test

import (
	"log"
	"net/http/httptest"
	"regexp"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"gitlab.com/softwareperonista/agenda-docente-backend/internal/data"
	"gitlab.com/softwareperonista/agenda-docente-backend/internal/models"
)

func createUserRepository() (data.UserRepository, sqlmock.Sqlmock) {
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	return data.UserRepository{Data: &data.Data{DB: db}}, mock
}

func TestUserGetAll(t *testing.T) {
	ur, mock := createUserRepository()

	mock.ExpectQuery(`SELECT id, first_name, last_name, username, email, picture FROM users;`).
		WillReturnRows(sqlmock.NewRows([]string{"id", "first_name", "last_name", "username", "email", "picture"}).
			AddRow("1", "First Name", "Last Name", "Username", "email@example.com", "http://example.com/picture.jpg").
			AddRow("2", "First Name 2", "Last Name 2", "Username2", "email2@example.com", "http://example.com/picture2.jpg"))

	r := httptest.NewRequest("GET", "http://example.com/foo", nil)

	ctx := r.Context()
	users, err := ur.GetAll(ctx)
	if err != nil {
		t.Error(err.Error())
	}

	if len(users) != 2 {
		t.Error("len(users): %user. Should be 2", len(users))
	}

	if users[0].Id != 1 ||
		users[0].FirstName != "First Name" ||
		users[0].LastName != "Last Name" ||
		users[0].Username != "Username" ||
		users[0].Email != "email@example.com" ||
		users[0].Picture != "http://example.com/picture.jpg" {
		t.Error("Wrong user[0] data")
	}

	if users[1].Id != 2 ||
		users[1].FirstName != "First Name 2" ||
		users[1].LastName != "Last Name 2" ||
		users[1].Username != "Username2" ||
		users[1].Email != "email2@example.com" ||
		users[1].Picture != "http://example.com/picture2.jpg" {
		t.Error("Wrong user[2] data")
	}
}

func TestUserGetOne(t *testing.T) {
	ur, mock := createUserRepository()

	mock.ExpectQuery(regexp.QuoteMeta(`SELECT id, first_name, last_name, username, email, picture FROM users WHERE id = $1;`)).
		WillReturnRows(sqlmock.NewRows([]string{"id", "first_name", "last_name", "username", "email", "picture"}).
			AddRow("1", "First Name", "Last Name", "Username", "email@example.com", "http://example.com/picture.jpg"))

	r := httptest.NewRequest("GET", "http://example.com/foo", nil)

	ctx := r.Context()
	user, err := ur.GetOne(ctx, 1)
	if err != nil {
		t.Error(err.Error())
	}

	if user.Id != 1 ||
		user.FirstName != "First Name" ||
		user.LastName != "Last Name" ||
		user.Username != "Username" ||
		user.Email != "email@example.com" ||
		user.Picture != "http://example.com/picture.jpg" {
		t.Error("Wrong user data")
	}
}

func TestUserGetByUsername(t *testing.T) {
	ur, mock := createUserRepository()

	mock.ExpectQuery(regexp.QuoteMeta(`SELECT id, first_name, last_name, username, email, picture, password FROM users WHERE username = $1;`)).
		WillReturnRows(sqlmock.NewRows([]string{"id", "first_name", "last_name", "username", "email", "picture", "password"}).
			AddRow("1", "First Name", "Last Name", "Username", "email@example.com", "http://example.com/picture.jpg", "Password"))

	r := httptest.NewRequest("GET", "http://example.com/foo", nil)

	ctx := r.Context()
	user, err := ur.GetByUsername(ctx, "Username")

	if err != nil {
		t.Error(err.Error())
	}

	if user.Id != 1 ||
		user.FirstName != "First Name" ||
		user.LastName != "Last Name" ||
		user.Username != "Username" ||
		user.Email != "email@example.com" ||
		user.Picture != "http://example.com/picture.jpg" ||
		user.PasswordHash != "Password" {
		t.Error("Wrong user data")
	}
}

func TestUserCreate(t *testing.T) {
	ur, mock := createUserRepository()

	mock.ExpectQuery(regexp.QuoteMeta(`INSERT INTO users (first_name, last_name, username, email, picture, password)
											VALUES ($1, $2, $3, $4, $5, $6)
											RETURNING id;`)).
		WillReturnRows(sqlmock.NewRows([]string{"id"}).
			AddRow("1"))

	r := httptest.NewRequest("GET", "http://example.com/foo", nil)

	user := models.User{
		FirstName: "First Name",
		LastName:  "Last Name",
		Username:  "Username",
		Email:     "email@example.com",
		Picture:   "http://example.com/picture.jpg",
		Password:  "Password",
	}

	ctx := r.Context()
	err := ur.Create(ctx, &user)
	if err != nil {
		t.Error(err.Error())
	}

	if user.PasswordHash == "" ||
		user.Id != 1 {
		t.Error("Wrong user data")
	}
}

func TestUserUpdate(t *testing.T) {
	ur, mock := createUserRepository()

	user := models.User{
		FirstName: "First Name",
		LastName:  "Last Name",
		Username:  "Username",
		Email:     "email@example.com",
		Picture:   "http://example.com/picture.jpg",
	}

	var id uint = 1
	mock.ExpectPrepare(regexp.QuoteMeta(`UPDATE users SET first_name=$1, last_name=$2, email=$3, picture=$4
											WHERE id = $5;`)).WillBeClosed().ExpectExec().WithArgs(user.FirstName, user.LastName, user.Email, user.Picture, id).WillReturnResult(sqlmock.NewResult(1, 1))

	r := httptest.NewRequest("GET", "http://example.com/foo", nil)

	ctx := r.Context()
	err := ur.Update(ctx, id, user)
	if err != nil {
		t.Error(err.Error())
	}

	err = mock.ExpectationsWereMet()
	if err != nil {
		t.Error(err.Error())
	}
}

func TestUserDelete(t *testing.T) {
	ur, mock := createUserRepository()

	var id uint = 1
	mock.ExpectPrepare(regexp.QuoteMeta(`DELETE FROM users WHERE id = $1;`)).WillBeClosed().ExpectExec().WithArgs(id).WillReturnResult(sqlmock.NewResult(1, 1))

	r := httptest.NewRequest("GET", "http://example.com/foo", nil)

	ctx := r.Context()
	err := ur.Delete(ctx, id)
	if err != nil {
		t.Error(err.Error())
	}

	err = mock.ExpectationsWereMet()
	if err != nil {
		t.Error(err.Error())
	}
}
