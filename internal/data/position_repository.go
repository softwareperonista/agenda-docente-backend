package data

import (
	"context"

	"gitlab.com/softwareperonista/agenda-docente-backend/internal/models"
)

type PositionRepository struct {
	Data *Data
}

func newScheduleRepository(data *Data) ScheduleRepository {
	return ScheduleRepository{Data: data}
}

func (pr *PositionRepository) GetAll(ctx context.Context) ([]models.Position, error) {
	q := `
		SELECT id, year, division, institution, teacher
		FROM positions;
	`

	rows, err := pr.Data.DB.QueryContext(ctx, q)
	if err != nil {
		return nil, err
	}

	var positions []models.Position
	sr := newScheduleRepository(pr.Data)
	for rows.Next() {
		var p models.Position
		rows.Scan(&p.Id, &p.Year, &p.Division, &p.Institution, &p.Teacher)
		schedules, err := sr.GetByPosition(ctx, p.Id)
		if err != nil {
			return nil, err
		}
		p.Schedule = schedules
		positions = append(positions, p)
	}

	return positions, nil
}

func (pr *PositionRepository) GetOne(ctx context.Context, id uint) (models.Position, error) {
	q := `
		SELECT id, year, division, institution, teacher
		FROM positions
		WHERE id = $1;
	`

	row := pr.Data.DB.QueryRowContext(ctx, q, id)

	var p models.Position
	err := row.Scan(&p.Id, &p.Year, &p.Division, &p.Institution, &p.Teacher)
	if err != nil {
		return models.Position{}, err
	}

	sr := newScheduleRepository(pr.Data)
	schedules, err := sr.GetByPosition(ctx, p.Id)
	if err != nil {
		return models.Position{}, err
	}
	p.Schedule = schedules

	return p, nil
}

func (pr *PositionRepository) GetByTeacher(ctx context.Context, tId uint) ([]models.Position, error) {
	q := `
		SELECT id, year, division, institution
		FROM positions
		WHERE teacher = $1;
	`

	rows, err := pr.Data.DB.QueryContext(ctx, q, tId)
	if err != nil {
		return nil, err
	}

	var positions []models.Position
	sr := newScheduleRepository(pr.Data)

	for rows.Next() {
		var p models.Position
		p.Teacher = tId
		rows.Scan(&p.Id, &p.Year, &p.Division, &p.Institution)
		schedules, err := sr.GetByPosition(ctx, p.Id)
		if err != nil {
			return nil, err
		}
		p.Schedule = schedules
		positions = append(positions, p)
	}

	return positions, nil
}

func (pr *PositionRepository) Create(ctx context.Context, p *models.Position) error {
	q := `
		INSERT INTO positions (year, division, institution, teacher)
		VALUES ($1, $2, $3, $4)
		RETURNING id;
	`

	tx, err := pr.Data.DB.BeginTx(ctx, nil)
	if err != nil {
		return err
	}

	row := tx.QueryRowContext(ctx, q, p.Year, p.Division, p.Institution, p.Teacher)

	err = row.Scan(&p.Id)
	if err != nil {
		tx.Rollback()
		return err
	}

	sr := newScheduleRepository(pr.Data)
	for _, s := range p.Schedule {
		s.Position = p.Id
		err = sr.Create(ctx, tx, &s)
		if err != nil {
			tx.Rollback()
			return err
		}
	}

	tx.Commit()
	return nil
}

func (pr *PositionRepository) Update(ctx context.Context, id uint, p models.Position) error {
	q := `
		UPDATE positions SET year = $1, division = $2, institution = $3, teacher = $4
		WHERE id = $5 
	`
	tx, err := pr.Data.DB.BeginTx(ctx, nil)
	if err != nil {
		return err
	}

	stmt, err := tx.PrepareContext(ctx, q)
	if err != nil {
		tx.Rollback()
		return err
	}

	_, err = stmt.ExecContext(ctx, p.Year, p.Division, p.Institution, p.Teacher)
	if err != nil {
		tx.Rollback()
		return err
	}

	tx.Commit()
	return nil
}

func (pr *PositionRepository) Delete(ctx context.Context, id uint) error {
	q := `
		DELETE FROM positions
			WHERE id = $1;
	`

	tx, err := pr.Data.DB.BeginTx(ctx, nil)
	if err != nil {
		return err
	}

	stmt, err := tx.PrepareContext(ctx, q)
	if err != nil {
		return err
	}

	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, id)
	if err != nil {
		return err
	}

	return nil
}
