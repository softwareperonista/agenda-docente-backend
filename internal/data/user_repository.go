package data

import (
	"context"

	"gitlab.com/softwareperonista/agenda-docente-backend/internal/models"
)

type UserRepository struct {
	Data *Data
}

func (ur *UserRepository) GetAll(ctx context.Context) ([]models.User, error) {
	q := `
		SELECT id, first_name, last_name, username, email, picture
			FROM users;
	`

	rows, err := ur.Data.DB.QueryContext(ctx, q)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	var users []models.User
	for rows.Next() {
		var u models.User
		rows.Scan(&u.Id, &u.FirstName, &u.LastName, &u.Username, &u.Email, &u.Picture)
		users = append(users, u)
	}

	return users, nil
}

func (ur *UserRepository) GetOne(ctx context.Context, id uint) (models.User, error) {
	q := `
		SELECT id, first_name, last_name, username, email, picture
			FROM users 
			WHERE id = $1;
	`

	row := ur.Data.DB.QueryRowContext(ctx, q, id)

	var u models.User
	err := row.Scan(&u.Id, &u.FirstName, &u.LastName, &u.Username, &u.Email, &u.Picture)
	if err != nil {
		return models.User{}, err
	}

	return u, nil
}

func (ur *UserRepository) GetByUsername(ctx context.Context, username string) (models.User, error) {
	q := `
		SELECT id, first_name, last_name, username, email, picture, password
			FROM users 
			WHERE username = $1;
	`

	row := ur.Data.DB.QueryRowContext(ctx, q, username)

	var u models.User
	err := row.Scan(&u.Id, &u.FirstName, &u.LastName, &u.Username, &u.Email, &u.Picture, &u.PasswordHash)
	if err != nil {
		return models.User{}, err
	}

	return u, nil
}

func (ur *UserRepository) Create(ctx context.Context, u *models.User) error {
	q := `
		INSERT INTO users (first_name, last_name, username, email, picture, password)
		VALUES ($1, $2, $3, $4, $5, $6)
		RETURNING id;
	`

	err := u.HashPassword()
	if err != nil {
		return err
	}

	row := ur.Data.DB.QueryRowContext(ctx, q, u.FirstName, u.LastName, u.Username, u.Email, u.Picture, u.PasswordHash)

	err = row.Scan(&u.Id)
	if err != nil {
		return err
	}

	return nil
}

func (ur *UserRepository) Update(ctx context.Context, id uint, u models.User) error {
	q := `
		UPDATE users SET first_name=$1, last_name=$2, email=$3, picture=$4
		WHERE id = $5;
	`
	stmt, err := ur.Data.DB.PrepareContext(ctx, q)
	if err != nil {
		return err
	}

	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, u.FirstName, u.LastName, u.Email, u.Picture, id)
	if err != nil {
		return err
	}

	return nil
}

func (ur *UserRepository) Delete(ctx context.Context, id uint) error {
	q := `
		DELETE FROM users WHERE id = $1;
	`

	stmt, err := ur.Data.DB.PrepareContext(ctx, q)
	if err != nil {
		return err
	}

	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, id)
	if err != nil {
		return err
	}

	return nil
}
