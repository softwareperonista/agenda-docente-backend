package data

import (
	"context"

	"gitlab.com/softwareperonista/agenda-docente-backend/internal/models"
)

type SchoolYearRepository struct {
	Data *Data
}

func (syr SchoolYearRepository) getSchoolYears(ctx context.Context, q string) ([]models.SchoolYear, error) {
	rows, err := syr.Data.DB.QueryContext(ctx, q)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	var schoolyears []models.SchoolYear
	for rows.Next() {
		var sy models.SchoolYear
		rows.Scan(&sy.Id, &sy.Year, &sy.Grade, &sy.FirstDay, &sy.LastDay, &sy.BreakStart, &sy.BreakEnd)
		schoolyears = append(schoolyears, sy)
	}

	return schoolyears, nil
}

func (syr *SchoolYearRepository) GetAll(ctx context.Context) ([]models.SchoolYear, error) {
	q := `
		SELECT id, year, grade, first_day, last_day, break_start, break_end 
			FROM schoolyears;
	`

	return syr.getSchoolYears(ctx, q)
}

func (syr SchoolYearRepository) GetOne(ctx context.Context, id uint) (models.SchoolYear, error) {
	q := `
		SELECT id, year, grade, first_day, last_day, break_start, break_end 
		FROM schoolyears
		WHERE id = $1;
	`

	row := syr.Data.DB.QueryRowContext(ctx, q, id)

	var sy models.SchoolYear
	err := row.Scan(&sy.Id, &sy.Year, &sy.Grade, &sy.FirstDay, &sy.LastDay, &sy.BreakStart, &sy.BreakEnd)
	if err != nil {
		return models.SchoolYear{}, err
	}

	return sy, nil
}

func (syr SchoolYearRepository) Create(ctx context.Context, s *models.SchoolYear) error {
	q := `
		INSERT INTO schoolyears (year, grade, first_day, last_day, break_start, break_end)
		VALUES($1, $2, $3, $4, $5, $6)
		RETURNING id;
	`

	row := syr.Data.DB.QueryRowContext(ctx, q, s.Year, s.Grade, s.FirstDay, s.LastDay, s.BreakStart, s.BreakEnd)

	err := row.Scan(&s.Id)
	if err != nil {
		return err
	}

	return nil
}

func (syr SchoolYearRepository) Update(ctx context.Context, id uint, s models.SchoolYear) error {
	q := `
		UPDATE schoolyears in year=$1, grade=$2, first_day=$3, last_day=$4, break_start=$5, break_end=$6
		WHERE id=$7;
	`

	stmt, err := syr.Data.DB.PrepareContext(ctx, q)
	if err != nil {
		return err
	}

	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, s.Year, s.Grade, s.FirstDay, s.LastDay, s.BreakStart, s.BreakEnd)
	if err != nil {
		return err
	}

	return nil
}

func (syr SchoolYearRepository) Delete(ctx context.Context, id uint) error {
	q := `
		DELETE FROM schoolyears WHERE id = $1;
	`

	stmt, err := syr.Data.DB.PrepareContext(ctx, q)
	if err != nil {
		return err
	}

	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, id)
	if err != nil {
		return err
	}

	return nil
}
