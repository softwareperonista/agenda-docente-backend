package models

import (
	"context"
)

type ScheduleRepository interface {
	GetByPosition(ctx context.Context, p uint) ([]Schedule, error)
	Create(ctx context.Context, s *Schedule) error
	Update(ctx context.Context, id uint, s Schedule) error
	Delete(ctx context.Context, id uint) error
}
