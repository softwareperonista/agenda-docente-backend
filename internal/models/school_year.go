package models

import "time"

type SchoolYear struct {
	Id         uint      `json:"id,omitempty"`
	Year       int       `json:"year,omitempty"`
	Grade      string    `json:"grade,omitempty"`
	FirstDay   time.Time `json:"first_day,omitempty"`
	LastDay    time.Time `json:"last_day,omitempty"`
	BreakStart time.Time `json:"break_start,omitempty"`
	BreakEnd   time.Time `json:"break_end,omitempty"`
}
