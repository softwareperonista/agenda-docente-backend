package models_test

import (
	"testing"

	"gitlab.com/softwareperonista/agenda-docente-backend/internal/models"
)

func TestHashPassword(t *testing.T) {
	u := models.User{
		Id:        1,
		FirstName: "First",
		LastName:  "Last",
		Username:  "User",
		Picture:   "",
		Email:     "e@mail.com",
		Password:  "password",
	}

	err := u.HashPassword()
	if err != nil {
		t.Errorf("%s", err)
	}

	if u.PasswordHash == "" {
		t.Errorf("PasswordHash is empty")
	}

	if u.PasswordHash == u.Password {
		t.Errorf("PasswordHash is equal to Password")
	}
}

func TestPasswordMatch(t *testing.T) {
	u := models.User{
		Id:        1,
		FirstName: "First",
		LastName:  "Last",
		Username:  "User",
		Picture:   "",
		Email:     "e@mail.com",
		Password:  "password",
	}

	err := u.HashPassword()
	if err != nil {
		t.Errorf("%s", err)
	}

	ok := u.PasswordMatch("password")
	if !ok {
		t.Errorf("Password cannot be validated")
	}
}

func TestPasswordMatchFail(t *testing.T) {
	u := models.User{
		Id:        1,
		FirstName: "First",
		LastName:  "Last",
		Username:  "User",
		Picture:   "",
		Email:     "e@mail.com",
		Password:  "password",
	}

	err := u.HashPassword()
	if err != nil {
		t.Errorf("%s", err)
	}

	ok := u.PasswordMatch("wrongpassword")
	if ok {
		t.Errorf("Wrong password validated")
	}
}
