package models

type Position struct {
	Id          uint       `json:"id,omitempty"`
	Year        string     `json:"year,omitempty"`
	Division    string     `json:"division,omitempty"`
	Institution string     `json:"institution,omitempty"`
	Schedule    []Schedule `json:"schedule,omitempty"`
	Teacher     uint       `json:"teacher,omitempty"`
}
