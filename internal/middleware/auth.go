package middleware

import (
	"context"
	"errors"
	"log"
	"net/http"
	"strings"

	"gitlab.com/softwareperonista/agenda-docente-backend/internal/server/response"
	"gitlab.com/softwareperonista/agenda-docente-backend/pkg/checkenv"
	"gitlab.com/softwareperonista/agenda-docente-backend/pkg/claim"
)

type key int

const (
	keyId key = iota
)

func Authorizator(next http.Handler) http.Handler {
	signingString, err := checkenv.Checkempty("SIGNING_STRING")
	if err != nil {
		log.Fatal(err)
	}

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authorization := r.Header.Get("Authorization")
		tokenString, err := tokenFromAuthorization(authorization)
		if err != nil {
			response.HTTPError(w, r, http.StatusUnauthorized, err.Error())
			return
		}

		c, err := claim.GetFromToken(tokenString, signingString)
		if err != nil {
			response.HTTPError(w, r, http.StatusUnauthorized, err.Error())
			return
		}

		ctx := r.Context()
		ctx = context.WithValue(ctx, keyId, c.Id)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func tokenFromAuthorization(authorization string) (string, error) {
	if authorization == "" {
		return "", errors.New("authorization is required")
	}

	if !strings.HasPrefix(authorization, "Bearer") {
		return "", errors.New("invalid authorization format1")
	}

	split := strings.Split(authorization, " ")
	if len(split) != 2 {
		return "", errors.New("invalid authorization format2")
	}

	return split[1], nil
}
