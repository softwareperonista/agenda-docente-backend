package v1

import (
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/softwareperonista/agenda-docente-backend/internal/middleware"
	"gitlab.com/softwareperonista/agenda-docente-backend/internal/models"
	"gitlab.com/softwareperonista/agenda-docente-backend/internal/server/response"
)

type PositionRouter struct {
	Repository models.PositionRepository
}

const PositionPath string = "/positions"

func (pr *PositionRouter) Routes() http.Handler {
	r := chi.NewRouter()

	r.With(middleware.Authorizator).Get("/", pr.GetAllHandler)
	r.With(middleware.Authorizator).Get("/{id}", pr.GetOneHandler)
	r.With(middleware.Authorizator).Get("/teacher/{id}", pr.GetByTeacherHandler)
	r.With(middleware.Authorizator).Post("/", pr.CreateHandler)
	r.With(middleware.Authorizator).Put("/{id}", pr.UpdateHandler)
	r.With(middleware.Authorizator).Delete("/{id}", pr.DeleteHandler)

	return r
}

func (pr *PositionRouter) GetAllHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	positions, err := pr.Repository.GetAll(ctx)
	if err != nil {
		response.HTTPError(w, r, http.StatusBadRequest, err.Error())
	}

	response.JSON(w, r, http.StatusOK, response.Map{"positions": positions})
}

func (pr *PositionRouter) GetOneHandler(w http.ResponseWriter, r *http.Request) {
	id, ok := getId(w, r)
	if !ok {
		return
	}

	ctx := r.Context()
	p, err := pr.Repository.GetOne(ctx, id)
	if err != nil {
		response.HTTPError(w, r, http.StatusNotFound, err.Error())
		return
	}

	response.JSON(w, r, http.StatusOK, response.Map{"position": p})
}

func (pr *PositionRouter) GetByTeacherHandler(w http.ResponseWriter, r *http.Request) {
	id, ok := getId(w, r)
	if !ok {
		return
	}

	ctx := r.Context()
	positions, err := pr.Repository.GetByTeacher(ctx, id)
	if err != nil {
		response.HTTPError(w, r, http.StatusNotFound, err.Error())
		return
	}

	response.JSON(w, r, http.StatusOK, response.Map{"positions": positions})
}

func (pr *PositionRouter) CreateHandler(w http.ResponseWriter, r *http.Request) {
	p, ok := decodePosition(w, r)
	if !ok {
		return
	}

	ctx := r.Context()
	err := pr.Repository.Create(ctx, &p)
	if err != nil {
		response.HTTPError(w, r, http.StatusNotFound, err.Error())
		return
	}

	response.JSON(w, r, http.StatusCreated, response.Map{"user": p})
}

func (pr *PositionRouter) UpdateHandler(w http.ResponseWriter, r *http.Request) {
	id, ok := getId(w, r)
	if !ok {
		return
	}

	p, ok := decodePosition(w, r)
	if !ok {
		return
	}

	ctx := r.Context()
	err := pr.Repository.Update(ctx, id, p)
	if err != nil {
		response.HTTPError(w, r, http.StatusNotFound, err.Error())
		return
	}

	response.JSON(w, r, http.StatusOK, nil)
}

func (pr *PositionRouter) DeleteHandler(w http.ResponseWriter, r *http.Request) {
	id, ok := getId(w, r)
	if !ok {
		return
	}

	ctx := r.Context()
	err := pr.Repository.Delete(ctx, id)
	if err != nil {
		response.HTTPError(w, r, http.StatusNotFound, err.Error())
		return
	}

	response.JSON(w, r, http.StatusOK, response.Map{})
}

func decodePosition(w http.ResponseWriter, r *http.Request) (models.Position, bool) {
	var p models.Position
	err := json.NewDecoder(r.Body).Decode(&p)
	if err != nil {
		response.HTTPError(w, r, http.StatusBadRequest, err.Error())
		return models.Position{}, false
	}

	defer r.Body.Close()

	return p, true
}
