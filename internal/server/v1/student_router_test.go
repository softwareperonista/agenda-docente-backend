package v1_test

import (
	"bytes"
	"errors"
	"log"
	"net/http"
	"net/http/httptest"
	"regexp"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/go-chi/chi"
	"gitlab.com/softwareperonista/agenda-docente-backend/internal/data"
	v1 "gitlab.com/softwareperonista/agenda-docente-backend/internal/server/v1"
)

const studentRouterPath string = v1.ApiPath + v1.StudentPath

func createStudentRouter() (v1.StudentRouter, sqlmock.Sqlmock) {
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	return v1.StudentRouter{Repository: &data.StudentRepository{Data: &data.Data{DB: db}}}, mock
}

func createStudentRouterTestServer(handlerRoute http.Handler) *httptest.Server {
	rUser := chi.NewRouter()
	rUser.Mount(v1.StudentPath, handlerRoute)
	r := chi.NewRouter()
	r.Mount(v1.ApiPath, rUser)

	return httptest.NewServer(r)
}

func TestStudentGetAllHandler(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		r := httptest.NewRequest("GET", "http://example.com"+studentRouterPath, nil)
		w := httptest.NewRecorder()

		sr, mock := createStudentRouter()

		mock.ExpectQuery(`SELECT id, first_name, last_name, gender, date_of_birth FROM students;`).
			WillReturnRows(sqlmock.NewRows([]string{"id", "first_name", "last_name", "gender", "date_of_birth"}).
				AddRow(1, "First Name", "Last Name", "Male", time.Date(1970, time.January, 01, 0, 0, 0, 0, time.UTC)).
				AddRow(2, "First Name 2", "Last Name 2", "Female", time.Date(1997, time.October, 10, 0, 0, 0, 0, time.UTC)))

		sr.GetAllHandler(w, r)

		res := w.Result()

		expectedStatusCode := http.StatusOK
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := `{"students":[{"id":1,"first_name":"First Name","last_name":"Last Name","gender":"Male","date_of_birth":"1970-01-01T00:00:00Z"},{"id":2,"first_name":"First Name 2","last_name":"Last Name 2","gender":"Female","date_of_birth":"1997-10-10T00:00:00Z"}]}`

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})

	t.Run("DB error", func(t *testing.T) {
		r := httptest.NewRequest("GET", "http://example.com"+studentRouterPath, nil)
		w := httptest.NewRecorder()

		sr, mock := createStudentRouter()

		mock.ExpectQuery(``)
		sr.GetAllHandler(w, r)

		res := w.Result()

		expectedStatusCode := http.StatusBadRequest
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := ""

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})
}

func TestStudentRouterGetOneHandler(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		sr, mock := createStudentRouter()

		route := chi.NewRouter()
		route.Get("/{id}", sr.GetOneHandler)

		ts := createStudentRouterTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("GET", ts.URL+studentRouterPath+"/1", nil)
		if err != nil {
			t.Fatal(err)
		}

		mock.ExpectQuery(regexp.QuoteMeta(`SELECT id, first_name, last_name, gender, date_of_birth FROM students WHERE id = $1;`)).
			WillReturnRows(sqlmock.NewRows([]string{"id", "first_name", "last_name", "gender", "date_of_birth"}).
				AddRow(1, "First Name", "Last Name", "Male", time.Date(1970, time.January, 01, 0, 0, 0, 0, time.UTC)))

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusOK
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := `{"student":{"id":1,"first_name":"First Name","last_name":"Last Name","gender":"Male","date_of_birth":"1970-01-01T00:00:00Z"}}`

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})

	t.Run("bad id", func(t *testing.T) {
		sr, _ := createStudentRouter()

		route := chi.NewRouter()
		route.Get("/{id}", sr.GetOneHandler)

		ts := createStudentRouterTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("GET", ts.URL+studentRouterPath+"/a", nil)
		if err != nil {
			t.Fatal(err)
		}

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusBadRequest
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := ""

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})

	t.Run("wrong id", func(t *testing.T) {
		sr, mock := createStudentRouter()

		route := chi.NewRouter()
		route.Get("/{id}", sr.GetOneHandler)

		ts := createStudentRouterTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("GET", ts.URL+studentRouterPath+"/2", nil)
		if err != nil {
			t.Fatal(err)
		}

		mock.ExpectQuery(regexp.QuoteMeta(`SELECT id, first_name, last_name, gender, date_of_birth FROM students WHERE id = $1;`)).
			WillReturnError(errors.New(""))

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusNotFound
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := ""

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})
}

func TestStudentCreateHandler(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		sr, mock := createStudentRouter()

		route := chi.NewRouter()
		route.Post("/", sr.CreateHandler)

		ts := createStudentRouterTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("POST", ts.URL+studentRouterPath, bytes.NewBufferString(`{"first_name":"First Name","last_name":"Last Name","gender":"Male","date_of_birth":"1970-01-01T00:00:00Z"}`))
		if err != nil {
			t.Fatal(err)
		}

		mock.ExpectQuery(regexp.QuoteMeta(`INSERT INTO students (first_name, last_name, gender, date_of_birth) VALUES ($1, $2, $3, $4) RETURNING id;`)).
			WillReturnRows(sqlmock.NewRows([]string{"id"}).
				AddRow(1))

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusCreated
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := `{"student":{"id":1,"first_name":"First Name","last_name":"Last Name","gender":"Male","date_of_birth":"1970-01-01T00:00:00Z"}}`

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})

	t.Run("malformed JSON", func(t *testing.T) {
		sr, _ := createStudentRouter()

		route := chi.NewRouter()
		route.Post("/", sr.CreateHandler)

		ts := createStudentRouterTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("POST", ts.URL+studentRouterPath, bytes.NewBufferString(`,"last_name":"Last Name","gender":"Male","date_of_birth":"1970-01-01T00:00:00Z"}`))
		if err != nil {
			t.Fatal(err)
		}

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusBadRequest
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := ""

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})

	t.Run("can't insert in DB", func(t *testing.T) {
		sr, mock := createStudentRouter()

		route := chi.NewRouter()
		route.Post("/", sr.CreateHandler)

		ts := createStudentRouterTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("POST", ts.URL+studentRouterPath, bytes.NewBufferString(`{"first_name":"First Name","last_name":"Last Name","gender":"Male","date_of_birth":"1970-01-01T00:00:00Z"}`))
		if err != nil {
			t.Fatal(err)
		}

		mock.ExpectQuery(regexp.QuoteMeta(`INSERT INTO students (first_name, last_name, gender, date_of_birth) VALUES ($1, $2, $3, $4) RETURNING id;`)).
			WillReturnError(errors.New(""))

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusBadRequest
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := ""

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})
}

func TestStudentUpdateHandler(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		sr, mock := createStudentRouter()

		route := chi.NewRouter()
		route.Put("/{id}", sr.UpdateHandler)

		ts := createStudentRouterTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("PUT", ts.URL+studentRouterPath+"/1", bytes.NewBufferString(`{"first_name":"First Name","last_name":"Last Name","gender":"Male","date_of_birth":"1970-01-01T00:00:00Z"}`))
		if err != nil {
			t.Fatal(err)
		}

		mock.ExpectPrepare(regexp.QuoteMeta(`UPDATE sutdents SET first_name = $1, last_name = $2, gender = $3, date_of_birth = $4 WHERE id = $5;`)).
			WillBeClosed().ExpectExec().WithArgs("First Name", "Last Name", "Male", time.Date(1970, time.January, 1, 0, 0, 0, 0, time.UTC), 1).WillReturnResult(sqlmock.NewResult(1, 1))

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusOK
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := ""

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})

	t.Run("bad id", func(t *testing.T) {
		sr, _ := createStudentRouter()

		route := chi.NewRouter()
		route.Put("/{id}", sr.UpdateHandler)

		ts := createStudentRouterTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("PUT", ts.URL+studentRouterPath+"/a", bytes.NewBufferString(`{"first_name":"First Name","last_name":"Last Name","gender":"Male","date_of_birth":"1970-01-01T00:00:00Z"}`))
		if err != nil {
			t.Fatal(err)
		}

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusBadRequest
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := ""

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})

	t.Run("wrong id", func(t *testing.T) {
		sr, _ := createStudentRouter()

		route := chi.NewRouter()
		route.Put("/{id}", sr.UpdateHandler)

		ts := createStudentRouterTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("PUT", ts.URL+studentRouterPath+"/1", bytes.NewBufferString(`{"first_name":"First Name","last_name":"Last Name","gender":"Male","date_of_birth":"1970-01-01T00:00:00Z"}`))
		if err != nil {
			t.Fatal(err)
		}

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusNotFound
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := ""

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})

	t.Run("malformed json", func(t *testing.T) {
		sr, _ := createStudentRouter()

		route := chi.NewRouter()
		route.Put("/{id}", sr.UpdateHandler)

		ts := createStudentRouterTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("PUT", ts.URL+studentRouterPath+"/1", bytes.NewBufferString(`{e","last_name":"Last Name","gender":"Male","date_of_birth":"1970-01-01T00:00:00Z"}`))
		if err != nil {
			t.Fatal(err)
		}

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusBadRequest
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := ""

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})
}

func TestStudentDeleteHandler(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		sr, mock := createStudentRouter()

		route := chi.NewRouter()
		route.Delete("/{id}", sr.DeleteHandler)

		ts := createStudentRouterTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("DELETE", ts.URL+studentRouterPath+"/1", nil)
		if err != nil {
			t.Fatal(err)
		}

		mock.ExpectPrepare(regexp.QuoteMeta(`DELETE FROM students WHERE id = $1;`)).
			WillBeClosed().ExpectExec().WithArgs(1).WillReturnResult(sqlmock.NewResult(1, 1))

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusOK
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := ""

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})

	t.Run("bad id", func(t *testing.T) {
		sr, _ := createStudentRouter()

		route := chi.NewRouter()
		route.Delete("/{id}", sr.DeleteHandler)

		ts := createStudentRouterTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("DELETE", ts.URL+studentRouterPath+"/a", nil)
		if err != nil {
			t.Fatal(err)
		}

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusBadRequest
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := ""

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})

	t.Run("wrong id", func(t *testing.T) {
		sr, mock := createStudentRouter()

		route := chi.NewRouter()
		route.Delete("/{id}", sr.DeleteHandler)

		ts := createStudentRouterTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("DELETE", ts.URL+studentRouterPath+"/1", nil)
		if err != nil {
			t.Fatal(err)
		}

		mock.ExpectPrepare(regexp.QuoteMeta(`DELETE FROM students WHERE id = $1;`)).
			WillBeClosed().ExpectExec().WithArgs(1).WillReturnError(errors.New(""))

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusNotFound
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := ""

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})
}
