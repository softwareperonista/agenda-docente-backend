package v1_test

import (
	"bytes"
	"errors"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"regexp"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/go-chi/chi"
	"gitlab.com/softwareperonista/agenda-docente-backend/internal/data"
	"gitlab.com/softwareperonista/agenda-docente-backend/internal/models"
	v1 "gitlab.com/softwareperonista/agenda-docente-backend/internal/server/v1"
)

const userRouterPath string = v1.ApiPath + v1.UserPath

func createUserRouter() (v1.UserRouter, sqlmock.Sqlmock) {
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	return v1.UserRouter{Repository: &data.UserRepository{Data: &data.Data{DB: db}}}, mock
}

func createUserRouteTestServer(handlerRoute http.Handler) *httptest.Server {
	rUser := chi.NewRouter()
	rUser.Mount(v1.UserPath, handlerRoute)
	r := chi.NewRouter()
	r.Mount(v1.ApiPath, rUser)

	return httptest.NewServer(r)
}

func TestUserRouterGetAllHandler(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		r := httptest.NewRequest("GET", userRouterPath, nil)
		w := httptest.NewRecorder()

		ur, mock := createUserRouter()

		mock.ExpectQuery(`SELECT id, first_name, last_name, username, email, picture FROM users;`).
			WillReturnRows(sqlmock.NewRows([]string{"id", "first_name", "last_name", "username", "email", "picture"}).
				AddRow("1", "First Name", "Last Name", "Username", "email@example.com", "http://example.com/picture.jpg").
				AddRow("2", "First Name 2", "Last Name 2", "Username2", "email2@example.com", "http://example.com/picture2.jpg"))

		ur.GetAllHandler(w, r)

		res := w.Result()

		expectedStatusCode := http.StatusOK
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := `{"users":[{"id":1,"first_name":"First Name","last_name":"Last Name","username":"Username","picture":"http://example.com/picture.jpg","email":"email@example.com"},{"id":2,"first_name":"First Name 2","last_name":"Last Name 2","username":"Username2","picture":"http://example.com/picture2.jpg","email":"email2@example.com"}]}`

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})

	t.Run("DB error", func(t *testing.T) {
		r := httptest.NewRequest("GET", userRouterPath, nil)
		w := httptest.NewRecorder()

		ur, mock := createUserRouter()

		mock.ExpectQuery(`SELECT id, first_name, last_name, username, email, picture FROM users;`)
		ur.GetAllHandler(w, r)

		res := w.Result()

		expectedStatusCode := http.StatusBadRequest
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := ""

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})
}

func TestUserRouterGetOneHandler(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		ur, mock := createUserRouter()

		route := chi.NewRouter()
		route.Get("/{id}", ur.GetOneHandler)

		ts := createUserRouteTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("GET", ts.URL+userRouterPath+"/1", nil)
		if err != nil {
			t.Fatal(err)
		}

		mock.ExpectQuery(regexp.QuoteMeta(`SELECT id, first_name, last_name, username, email, picture FROM users WHERE id = $1;`)).
			WillReturnRows(sqlmock.NewRows([]string{"id", "first_name", "last_name", "username", "email", "picture"}).
				AddRow("1", "First Name", "Last Name", "Username", "email@example.com", "http://example.com/picture.jpg"))

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusOK
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := `{"user":{"id":1,"first_name":"First Name","last_name":"Last Name","username":"Username","picture":"http://example.com/picture.jpg","email":"email@example.com"}}`

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})

	t.Run("bad id", func(t *testing.T) {
		ur, mock := createUserRouter()

		route := chi.NewRouter()
		route.Get("/{id}", ur.GetOneHandler)

		ts := createUserRouteTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("GET", ts.URL+userRouterPath+"/a", nil)
		if err != nil {
			t.Fatal(err)
		}

		mock.ExpectQuery(regexp.QuoteMeta(`SELECT id, first_name, last_name, username, email, picture FROM users WHERE id = $1;`)).
			WillReturnRows(sqlmock.NewRows([]string{"id", "first_name", "last_name", "username", "email", "picture"}).
				AddRow("1", "First Name", "Last Name", "Username", "email@example.com", "http://example.com/picture.jpg"))

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusBadRequest
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := ""

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})

	t.Run("wrong id", func(t *testing.T) {
		ur, mock := createUserRouter()

		route := chi.NewRouter()
		route.Get("/{id}", ur.GetOneHandler)

		ts := createUserRouteTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("GET", ts.URL+userRouterPath+"/2", nil)
		if err != nil {
			t.Fatal(err)
		}

		mock.ExpectQuery(regexp.QuoteMeta(`SELECT id, first_name, last_name, username, email, picture FROM users WHERE id = $1;`))

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusNotFound
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := ""

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})
}

func TestUserRouterCreateHandler(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		ur, mock := createUserRouter()

		route := chi.NewRouter()
		route.Post("/", ur.CreateHandler)

		ts := createUserRouteTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("POST", ts.URL+userRouterPath, bytes.NewBufferString(`{"first_name":"First Name","last_name":"Last Name","username":"Username","picture":"http://example.com/picture.jpg","email":"email@example.com","password":"password"}`))
		if err != nil {
			t.Fatal(err)
		}

		mock.ExpectQuery(regexp.QuoteMeta(`INSERT INTO users (first_name, last_name, username, email, picture, password)
											VALUES ($1, $2, $3, $4, $5, $6)
											RETURNING id;`)).
			WillReturnRows(sqlmock.NewRows([]string{"id"}).
				AddRow("1"))

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusCreated
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := `{"user":{"id":1,"first_name":"First Name","last_name":"Last Name","username":"Username","picture":"http://example.com/picture.jpg","email":"email@example.com"}}`

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})

	t.Run("malformed JSON", func(t *testing.T) {
		ur, mock := createUserRouter()

		route := chi.NewRouter()
		route.Post("/", ur.CreateHandler)

		ts := createUserRouteTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("POST", ts.URL+userRouterPath, bytes.NewBufferString(`"last_name":"Last Name","username":"Username","picture":"http://example.com/picture.jpg","email":"email@example.com","password":"password"}`))
		if err != nil {
			t.Fatal(err)
		}

		mock.ExpectQuery(regexp.QuoteMeta(`INSERT INTO users (first_name, last_name, username, email, picture, password)
											VALUES ($1, $2, $3, $4, $5, $6)
											RETURNING id;`)).
			WillReturnRows(sqlmock.NewRows([]string{"id"}).
				AddRow("1"))

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusBadRequest
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := ""

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})

	t.Run("can't insert in DB", func(t *testing.T) {
		ur, mock := createUserRouter()

		route := chi.NewRouter()
		route.Post("/", ur.CreateHandler)

		ts := createUserRouteTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("POST", ts.URL+userRouterPath, bytes.NewBufferString(`{"user":{"id":1,"first_name":"First Name","last_name":"Last Name","username":"Username","picture":"http://example.com/picture.jpg","email":"email@example.com"}}`))
		if err != nil {
			t.Fatal(err)
		}

		mock.ExpectQuery(regexp.QuoteMeta(`INSERT INTO users (first_name, last_name, username, email, picture, password)
											VALUES ($1, $2, $3, $4, $5, $6)
											RETURNING id;`)).
			WillReturnError(errors.New(""))

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusBadRequest
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := ""

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})

	t.Run("username repeated", func(t *testing.T) {
		ur, mock := createUserRouter()

		route := chi.NewRouter()
		route.Post("/", ur.CreateHandler)

		ts := createUserRouteTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("POST", ts.URL+userRouterPath, bytes.NewBufferString(`{"user":{"id":1,"first_name":"First Name","last_name":"Last Name","username":"Username","picture":"http://example.com/picture.jpg","email":"email@example.com"}}`))
		if err != nil {
			t.Fatal(err)
		}

		mock.ExpectQuery(regexp.QuoteMeta(`INSERT INTO users (first_name, last_name, username, email, picture, password)
											VALUES ($1, $2, $3, $4, $5, $6)
											RETURNING id;`)).
			WillReturnError(errors.New(`pq: duplicate key value violates unique constraint "users_username_key"`))

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusBadRequest
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := `{"message":"Username already exists"}`

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})

	t.Run("email repeated", func(t *testing.T) {
		ur, mock := createUserRouter()

		route := chi.NewRouter()
		route.Post("/", ur.CreateHandler)

		ts := createUserRouteTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("POST", ts.URL+userRouterPath, bytes.NewBufferString(`{"user":{"id":1,"first_name":"First Name","last_name":"Last Name","username":"Username","picture":"http://example.com/picture.jpg","email":"email@example.com"}}`))
		if err != nil {
			t.Fatal(err)
		}

		mock.ExpectQuery(regexp.QuoteMeta(`INSERT INTO users (first_name, last_name, username, email, picture, password)
											VALUES ($1, $2, $3, $4, $5, $6)
											RETURNING id;`)).
			WillReturnError(errors.New(`pq: duplicate key value violates unique constraint "users_email_key"`))

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusBadRequest
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := `{"message":"Email already used"}`

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})
}

func TestUserRouterUpdateHandler(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		ur, mock := createUserRouter()

		route := chi.NewRouter()
		route.Put("/{id}", ur.UpdateHandler)

		ts := createUserRouteTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("PUT", ts.URL+userRouterPath+"/1", bytes.NewBufferString(`{"first_name":"First Name","last_name":"Last Name","username":"Username","picture":"http://example.com/picture.jpg","email":"email@example.com","password":"password"}`))
		if err != nil {
			t.Fatal(err)
		}

		mock.ExpectPrepare(regexp.QuoteMeta(`UPDATE users SET first_name=$1, last_name=$2, email=$3, picture=$4
		WHERE id = $5;`)).WillBeClosed().ExpectExec().WithArgs("First Name", "Last Name", "email@example.com", "http://example.com/picture.jpg", 1).WillReturnResult(sqlmock.NewResult(1, 1))

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusOK
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := ""

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})

	t.Run("bad id", func(t *testing.T) {
		ur, mock := createUserRouter()

		route := chi.NewRouter()
		route.Put("/{id}", ur.UpdateHandler)

		ts := createUserRouteTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("PUT", ts.URL+userRouterPath+"/2", bytes.NewBufferString(`{"first_name":"First Name","last_name":"Last Name","username":"Username","picture":"http://example.com/picture.jpg","email":"email@example.com","password":"password"}`))
		if err != nil {
			t.Fatal(err)
		}

		mock.ExpectPrepare(regexp.QuoteMeta(`UPDATE users SET first_name=$1, last_name=$2, email=$3, picture=$4
		WHERE id = $5;`)).WillReturnError(errors.New(""))

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusNotFound
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := ""

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})

	t.Run("wrong id", func(t *testing.T) {
		ur, mock := createUserRouter()

		route := chi.NewRouter()
		route.Put("/{id}", ur.UpdateHandler)

		ts := createUserRouteTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("PUT", ts.URL+userRouterPath+"/a", bytes.NewBufferString(`{"first_name":"First Name","last_name":"Last Name","username":"Username","picture":"http://example.com/picture.jpg","email":"email@example.com","password":"password"}`))
		if err != nil {
			t.Fatal(err)
		}

		mock.ExpectPrepare(regexp.QuoteMeta(`UPDATE users SET first_name=$1, last_name=$2, email=$3, picture=$4
		WHERE id = $5;`)).WillBeClosed().ExpectExec().WithArgs("First Name", "Last Name", "email@example.com", "http://example.com/picture.jpg", 1).WillReturnResult(sqlmock.NewResult(1, 1))

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusBadRequest
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := ""

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})

	t.Run("malformed JSON", func(t *testing.T) {
		ur, mock := createUserRouter()

		route := chi.NewRouter()
		route.Put("/{id}", ur.UpdateHandler)

		ts := createUserRouteTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("PUT", ts.URL+userRouterPath+"/1", bytes.NewBufferString(`Name","last_name":"Last Name","username":"Username","picture":"http://example.com/picture.jpg","email":"email@example.com","password":"password"}`))
		if err != nil {
			t.Fatal(err)
		}

		mock.ExpectPrepare(regexp.QuoteMeta(`UPDATE users SET first_name=$1, last_name=$2, email=$3, picture=$4
		WHERE id = $5;`)).WillBeClosed().ExpectExec().WithArgs("First Name", "Last Name", "email@example.com", "http://example.com/picture.jpg", 1).WillReturnResult(sqlmock.NewResult(1, 1))

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusBadRequest
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := ""

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})
}

func TestUserRouterDeleteHandler(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		ur, mock := createUserRouter()

		route := chi.NewRouter()
		route.Delete("/{id}", ur.DeleteHandler)

		ts := createUserRouteTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("DELETE", ts.URL+userRouterPath+"/1", nil)
		if err != nil {
			t.Fatal(err)
		}

		mock.ExpectPrepare(regexp.QuoteMeta(`DELETE FROM users WHERE id = $1;`)).WillBeClosed().ExpectExec().WithArgs(1).WillReturnResult(sqlmock.NewResult(1, 1))

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusOK
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := ""

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})

	t.Run("wrong id", func(t *testing.T) {
		ur, mock := createUserRouter()

		route := chi.NewRouter()
		route.Delete("/{id}", ur.DeleteHandler)

		ts := createUserRouteTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("DELETE", ts.URL+userRouterPath+"/2", nil)
		if err != nil {
			t.Fatal(err)
		}

		mock.ExpectPrepare(regexp.QuoteMeta(`DELETE FROM users WHERE id = $1;`)).WillBeClosed().ExpectExec().WithArgs(1).WillReturnResult(sqlmock.NewResult(1, 1))

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusNotFound
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := ""

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})

	t.Run("bad id", func(t *testing.T) {
		ur, mock := createUserRouter()

		route := chi.NewRouter()
		route.Delete("/{id}", ur.DeleteHandler)

		ts := createUserRouteTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("DELETE", ts.URL+userRouterPath+"/a", nil)
		if err != nil {
			t.Fatal(err)
		}

		mock.ExpectPrepare(regexp.QuoteMeta(`DELETE FROM users WHERE id = $1;`)).WillReturnError(errors.New(""))

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusBadRequest
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := ""

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})
}

func TestUserRouterLoginHandler(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		ur, mock := createUserRouter()

		route := chi.NewRouter()
		route.Post("/login/", ur.LoginHandler)

		ts := createUserRouteTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("POST", ts.URL+userRouterPath+"/login/", bytes.NewBufferString(`{"username":"Username","password":"password"}`))
		if err != nil {
			t.Fatal(err)
		}

		u := models.User{Password: "password"}
		u.HashPassword()

		os.Setenv("SIGNING_STRING", "asd123")

		mock.ExpectQuery(regexp.QuoteMeta(`SELECT id, first_name, last_name, username, email, picture, password FROM users WHERE username = $1;`)).
			WillReturnRows(sqlmock.NewRows([]string{"id", "first_name", "last_name", "username", "email", "picture", "password"}).
				AddRow("1", "First Name", "Last Name", "Username", "email@example.com", "http://example.com/picture.jpg", u.PasswordHash))

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusOK
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := ""

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})

	t.Run("malformed JSON", func(t *testing.T) {
		ur, mock := createUserRouter()

		route := chi.NewRouter()
		route.Post("/login/", ur.LoginHandler)

		ts := createUserRouteTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("POST", ts.URL+userRouterPath+"/login/", bytes.NewBufferString(`ername":"Username","password":"password"}`))
		if err != nil {
			t.Fatal(err)
		}

		u := models.User{Password: "password"}
		u.HashPassword()

		os.Setenv("SIGNING_STRING", "asd123")

		mock.ExpectQuery(regexp.QuoteMeta(`SELECT id, first_name, last_name, username, email, picture, password FROM users WHERE username = $1;`)).
			WillReturnRows(sqlmock.NewRows([]string{"id", "first_name", "last_name", "username", "email", "picture", "password"}).
				AddRow("1", "First Name", "Last Name", "Username", "email@example.com", "http://example.com/picture.jpg", u.PasswordHash))

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusBadRequest
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := ""

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})

	t.Run("wrong username", func(t *testing.T) {
		ur, mock := createUserRouter()

		route := chi.NewRouter()
		route.Post("/login/", ur.LoginHandler)

		ts := createUserRouteTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("POST", ts.URL+userRouterPath+"/login/", bytes.NewBufferString(`{"username":"wrong","password":"password"}`))
		if err != nil {
			t.Fatal(err)
		}

		u := models.User{Password: "password"}
		u.HashPassword()

		os.Setenv("SIGNING_STRING", "asd123")

		mock.ExpectQuery(regexp.QuoteMeta(`SELECT id, first_name, last_name, username, email, picture, password FROM users WHERE username = $1;`)).
			WillReturnError(errors.New(""))

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusNotFound
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := ""

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})

	t.Run("wrong passord", func(t *testing.T) {
		ur, mock := createUserRouter()

		route := chi.NewRouter()
		route.Post("/login/", ur.LoginHandler)

		ts := createUserRouteTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("POST", ts.URL+userRouterPath+"/login/", bytes.NewBufferString(`{"username":"wrong","password":"pass"}`))
		if err != nil {
			t.Fatal(err)
		}

		u := models.User{Password: "password"}
		u.HashPassword()

		os.Setenv("SIGNING_STRING", "asd123")

		mock.ExpectQuery(regexp.QuoteMeta(`SELECT id, first_name, last_name, username, email, picture, password FROM users WHERE username = $1;`)).
			WillReturnRows(sqlmock.NewRows([]string{"id", "first_name", "last_name", "username", "email", "picture", "password"}).
				AddRow("1", "First Name", "Last Name", "Username", "email@example.com", "http://example.com/picture.jpg", u.PasswordHash))

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusBadRequest
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := ""

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})
}
