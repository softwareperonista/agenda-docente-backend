package v1_test

import (
	"io"
	"net/http"
	"testing"
)

func httpResultTestHelper(t *testing.T, res *http.Response, ExpectedstatusCode int, ExpectedcontentType string, Expectedbody string) {
	t.Helper()

	checkTest(t, "wrong statusCode", ExpectedstatusCode, res.StatusCode)

	checkTest(t, "wrong Content-Type", ExpectedcontentType, res.Header.Get("Content-Type"))

	if Expectedbody != "" {
		resBody, _ := io.ReadAll(res.Body)
		checkTest(t, "wrong body", Expectedbody, string(resBody))
	}
}

func checkTest(t *testing.T, message string, expected interface{}, result interface{}) {
	t.Helper()

	if expected != result {
		t.Errorf("%s. Expected '%v', got '%v'", message, expected, result)
	}
}
