package v1

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/softwareperonista/agenda-docente-backend/internal/middleware"
	"gitlab.com/softwareperonista/agenda-docente-backend/internal/models"
	"gitlab.com/softwareperonista/agenda-docente-backend/internal/server/response"
	"gitlab.com/softwareperonista/agenda-docente-backend/pkg/checkenv"
	"gitlab.com/softwareperonista/agenda-docente-backend/pkg/claim"
)

type UserRouter struct {
	Repository models.UserRepository
}

const UserPath string = "/users"

func (ur *UserRouter) Routes() http.Handler {
	r := chi.NewRouter()

	r.With(middleware.Authorizator).Get("/", ur.GetAllHandler)
	r.With(middleware.Authorizator).Get("/{id}", ur.GetOneHandler)
	r.With(middleware.Authorizator).Put("/{id}", ur.UpdateHandler)
	r.With(middleware.Authorizator).Delete("/{id}", ur.DeleteHandler)
	r.Post("/", ur.CreateHandler)
	r.Post("/login/", ur.LoginHandler)

	return r
}

func (ur *UserRouter) GetAllHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	users, err := ur.Repository.GetAll(ctx)
	if err != nil {
		response.HTTPError(w, r, http.StatusBadRequest, err.Error())
		return
	}

	response.JSON(w, r, http.StatusOK, response.Map{"users": users})
}

func (ur *UserRouter) GetOneHandler(w http.ResponseWriter, r *http.Request) {
	id, ok := getId(w, r)
	if !ok {
		return
	}

	ctx := r.Context()
	u, err := ur.Repository.GetOne(ctx, id)
	if err != nil {
		response.HTTPError(w, r, http.StatusNotFound, err.Error())
		return
	}

	response.JSON(w, r, http.StatusOK, response.Map{"user": u})
}

func (ur *UserRouter) CreateHandler(w http.ResponseWriter, r *http.Request) {
	u, ok := decodeUser(w, r)
	if !ok {
		return
	}

	ctx := r.Context()
	err := ur.Repository.Create(ctx, &u)
	if err != nil {
		e := err.Error()
		switch err.Error() {
		case `pq: duplicate key value violates unique constraint "users_username_key"`:
			{
				e = "Username already exists"
			}
		case `pq: duplicate key value violates unique constraint "users_email_key"`:
			{
				e = "Email already used"
			}
		}

		response.HTTPError(w, r, http.StatusBadRequest, e)
		return
	}

	u.Password = ""
	w.Header().Add("Location", fmt.Sprintf("%s%d", r.URL.String(), u.Id))
	response.JSON(w, r, http.StatusCreated, response.Map{"user": u})
}

func (ur *UserRouter) UpdateHandler(w http.ResponseWriter, r *http.Request) {
	id, ok := getId(w, r)
	if !ok {
		return
	}

	u, ok := decodeUser(w, r)
	if !ok {
		return
	}

	ctx := r.Context()
	err := ur.Repository.Update(ctx, id, u)
	if err != nil {
		response.HTTPError(w, r, http.StatusNotFound, err.Error())
		return
	}

	response.JSON(w, r, http.StatusOK, nil)
}

func (ur *UserRouter) DeleteHandler(w http.ResponseWriter, r *http.Request) {
	id, ok := getId(w, r)
	if !ok {
		return
	}

	ctx := r.Context()
	err := ur.Repository.Delete(ctx, id)
	if err != nil {
		response.HTTPError(w, r, http.StatusNotFound, err.Error())
		return
	}

	response.JSON(w, r, http.StatusOK, response.Map{})
}

func (ur *UserRouter) LoginHandler(w http.ResponseWriter, r *http.Request) {
	u, ok := decodeUser(w, r)
	if !ok {
		return
	}

	ctx := r.Context()
	storedUser, err := ur.Repository.GetByUsername(ctx, u.Username)
	if err != nil {
		response.HTTPError(w, r, http.StatusNotFound, err.Error())
		return
	}

	if !storedUser.PasswordMatch(u.Password) {
		response.HTTPError(w, r, http.StatusBadRequest, "password don't match")
		return
	}

	c := claim.Claim{Id: int(storedUser.Id)}
	signingString, err := checkenv.Checkempty("SIGNING_STRING")
	if err != nil {
		log.Fatal(err)
	}
	token, err := c.GetToken(signingString)
	if err != nil {
		response.HTTPError(w, r, http.StatusInternalServerError, err.Error())
	}

	response.JSON(w, r, http.StatusOK, response.Map{"token": token})
}

func decodeUser(w http.ResponseWriter, r *http.Request) (models.User, bool) {
	var u models.User
	err := json.NewDecoder(r.Body).Decode(&u)
	if err != nil {
		response.HTTPError(w, r, http.StatusBadRequest, err.Error())
		return models.User{}, false
	}

	defer r.Body.Close()

	return u, true
}
