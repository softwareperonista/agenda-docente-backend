package v1_test

import (
	"bytes"
	"log"
	"net/http"
	"net/http/httptest"
	"regexp"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/go-chi/chi"
	"gitlab.com/softwareperonista/agenda-docente-backend/internal/data"
	v1 "gitlab.com/softwareperonista/agenda-docente-backend/internal/server/v1"
)

func createSchoolYearRouter() (v1.SchoolYearRouter, sqlmock.Sqlmock) {
	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
	}

	return v1.SchoolYearRouter{Repository: &data.SchoolYearRepository{Data: &data.Data{DB: db}}}, mock
}

const schoolYearRouterPath string = v1.ApiPath + v1.SchoolYearPath

func createSchoolYearRouterTestServer(handlerRoute http.Handler) *httptest.Server {
	rSchoolYear := chi.NewRouter()
	rSchoolYear.Mount(v1.SchoolYearPath, handlerRoute)
	r := chi.NewRouter()
	r.Mount(v1.ApiPath, rSchoolYear)

	return httptest.NewServer(r)
}

func TestSchoolYearRouterGetAllHandler(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		r := httptest.NewRequest("GET", schoolYearRouterPath, nil)
		w := httptest.NewRecorder()

		syr, mock := createSchoolYearRouter()

		mock.ExpectQuery(`SELECT id, year, grade, first_day, last_day, break_start, break_end 
    FROM schoolyears;`).
			WillReturnRows(sqlmock.NewRows([]string{"id", "year", "grade", "first_day", "last_day", "break_start", "break_end"}).
				AddRow(
					1,
					2021,
					"Secundaria",
					time.Date(2021, time.March, 1, 0, 0, 0, 0, time.UTC),
					time.Date(2021, time.December, 20, 0, 0, 0, 0, time.UTC),
					time.Date(2021, time.July, 9, 0, 0, 0, 0, time.UTC),
					time.Date(2021, time.July, 25, 0, 0, 0, 0, time.UTC)).
				AddRow(
					2,
					2021,
					"Universidad",
					time.Date(2021, time.March, 2, 0, 0, 0, 0, time.UTC),
					time.Date(2021, time.December, 21, 0, 0, 0, 0, time.UTC),
					time.Date(2021, time.July, 10, 0, 0, 0, 0, time.UTC),
					time.Date(2021, time.July, 26, 0, 0, 0, 0, time.UTC)))

		syr.GetAllHandler(w, r)

		res := w.Result()

		expectedStatusCode := http.StatusOK
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := `{"schoolyears":[{"id":1,"year":2021,"grade":"Secundaria","first_day":"2021-03-01T00:00:00Z","last_day":"2021-12-20T00:00:00Z","break_start":"2021-07-09T00:00:00Z","break_end":"2021-07-25T00:00:00Z"},{"id":2,"year":2021,"grade":"Universidad","first_day":"2021-03-02T00:00:00Z","last_day":"2021-12-21T00:00:00Z","break_start":"2021-07-10T00:00:00Z","break_end":"2021-07-26T00:00:00Z"}]}`

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})

	t.Run("DB error", func(t *testing.T) {
		r := httptest.NewRequest("GET", schoolYearRouterPath, nil)
		w := httptest.NewRecorder()

		syr, mock := createSchoolYearRouter()

		mock.ExpectQuery(`SELECT id, year, grade, first_day, last_day, break_start, break_end 
    FROM schoolyears;`)
		syr.GetAllHandler(w, r)

		res := w.Result()

		expectedStatusCode := http.StatusBadRequest
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := ""

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})
}

func TestSchoolYearRouterGetOneHandler(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		syr, mock := createSchoolYearRouter()

		route := chi.NewRouter()
		route.Get("/{id}", syr.GetOneHandler)

		ts := createSchoolYearRouterTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("GET", ts.URL+schoolYearRouterPath+"/1", nil)
		if err != nil {
			t.Fatal(err)
		}

		mock.ExpectQuery(regexp.QuoteMeta(
			`SELECT id, year, grade, first_day, last_day, break_start, break_end 
				FROM schoolyears
				WHERE id = $1;`)).
			WillReturnRows(sqlmock.NewRows([]string{"id", "year", "grade", "first_day", "last_day", "break_start", "break_end"}).
				AddRow(
					1,
					2021,
					"Secundaria",
					time.Date(2021, time.March, 1, 0, 0, 0, 0, time.UTC),
					time.Date(2021, time.December, 20, 0, 0, 0, 0, time.UTC),
					time.Date(2021, time.July, 9, 0, 0, 0, 0, time.UTC),
					time.Date(2021, time.July, 25, 0, 0, 0, 0, time.UTC)))

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusOK
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := `{"schoolyear":{"id":1,"year":2021,"grade":"Secundaria","first_day":"2021-03-01T00:00:00Z","last_day":"2021-12-20T00:00:00Z","break_start":"2021-07-09T00:00:00Z","break_end":"2021-07-25T00:00:00Z"}}`

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})

	t.Run("wrong id", func(t *testing.T) {
		syr, mock := createSchoolYearRouter()

		route := chi.NewRouter()
		route.Get("/{id}", syr.GetOneHandler)

		ts := createSchoolYearRouterTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("GET", ts.URL+schoolYearRouterPath+"/2", nil)
		if err != nil {
			t.Fatal(err)
		}

		mock.ExpectQuery(regexp.QuoteMeta(
			`SELECT id, year, grade, first_day, last_day, break_start, break_end 
				FROM schoolyears
				WHERE id = $1;`))

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusNotFound
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := ""

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})

	t.Run("wrong id", func(t *testing.T) {
		syr, mock := createSchoolYearRouter()

		route := chi.NewRouter()
		route.Get("/{id}", syr.GetOneHandler)

		ts := createSchoolYearRouterTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("GET", ts.URL+schoolYearRouterPath+"/a", nil)
		if err != nil {
			t.Fatal(err)
		}

		mock.ExpectQuery(regexp.QuoteMeta(
			`SELECT id, year, grade, first_day, last_day, break_start, break_end 
				FROM schoolyears
				WHERE id = $1;`))

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusBadRequest
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := ""

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})
}

func TestSchoolYearRouterCreateHandler(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		syr, mock := createSchoolYearRouter()

		route := chi.NewRouter()
		route.Post("/", syr.Createandler)

		ts := createSchoolYearRouterTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("POST", ts.URL+schoolYearRouterPath, bytes.NewBufferString(`{"year":2021,"grade":"Secundaria","first_day":"2021-03-01T00:00:00Z","last_day":"2021-12-20T00:00:00Z","break_start":"2021-07-09T00:00:00Z","break_end":"2021-07-25T00:00:00Z"}`))
		if err != nil {
			t.Fatal(err)
		}

		mock.ExpectQuery(regexp.QuoteMeta(`INSERT INTO schoolyears (year, grade, first_day, last_day, break_start, break_end)
										VALUES($1, $2, $3, $4, $5, $6)
										RETURNING id`)).
			WillReturnRows(sqlmock.NewRows([]string{"id"}).
				AddRow(1))

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusCreated
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := `{"schoolyear":{"id":1,"year":2021,"grade":"Secundaria","first_day":"2021-03-01T00:00:00Z","last_day":"2021-12-20T00:00:00Z","break_start":"2021-07-09T00:00:00Z","break_end":"2021-07-25T00:00:00Z"}}`

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})

	t.Run("malformed JSON", func(t *testing.T) {
		syr, mock := createSchoolYearRouter()

		route := chi.NewRouter()
		route.Post("/", syr.Createandler)

		ts := createSchoolYearRouterTestServer(route)
		defer ts.Close()

		r, err := http.NewRequest("POST", ts.URL+schoolYearRouterPath, bytes.NewBufferString(`{"ye1,"grade":"Secundaria","first_day":"2021-03-01T00:00:00Z","last_day":"2021-12-20T00:00:00Z","break_start":"2021-07-09T00:00:00Z","break_end":"2021-07-25T00:00:00Z"}`))
		if err != nil {
			t.Fatal(err)
		}

		mock.ExpectQuery(regexp.QuoteMeta(`INSERT INTO schoolyears (year, grade, first_day, last_day, break_start, break_end)
										VALUES($1, $2, $3, $4, $5, $6)
										RETURNING id`)).
			WillReturnRows(sqlmock.NewRows([]string{"id"}).
				AddRow(1))

		res, err := ts.Client().Do(r)
		if err != nil {
			t.Fatal(err)
		}

		expectedStatusCode := http.StatusBadRequest
		expectedContentType := "application/json; charset=utf-8"
		expectedBody := ""

		httpResultTestHelper(t, res, expectedStatusCode, expectedContentType, expectedBody)
	})
}
