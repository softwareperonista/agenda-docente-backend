package v1

import (
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"gitlab.com/softwareperonista/agenda-docente-backend/internal/data"
	"gitlab.com/softwareperonista/agenda-docente-backend/internal/server/response"
)

const ApiPath string = "/api/v1"

func New() http.Handler {
	r := chi.NewRouter()

	ur := &UserRouter{Repository: &data.UserRepository{Data: data.New()}}
	r.Mount(UserPath, ur.Routes())

	sr := &StudentRouter{Repository: &data.StudentRepository{Data: data.New()}}
	r.Mount(StudentPath, sr.Routes())

	pr := &PositionRouter{Repository: &data.PositionRepository{Data: data.New()}}
	r.Mount(PositionPath, pr.Routes())

	syr := SchoolYearRouter{Repository: &data.SchoolYearRepository{Data: data.New()}}
	r.Mount(SchoolYearPath, syr.Routes())

	return r
}

func getId(w http.ResponseWriter, r *http.Request) (uint, bool) {
	id, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		response.HTTPError(w, r, http.StatusBadRequest, err.Error())
		return 0, false
	}

	return uint(id), true
}
